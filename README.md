# What's that whistle?
This project is about challenging other users users to find out the song that some random whistle is trying to replicate. Within the app, users can record and upload whistles, which other users can download, preview (hear) and make suggestions on. Users can also select certain music genres to subscribe to, and be notified whenevera new whistle is uploaded, for any of those genres.

### Relevant technologies
- AVFoundation for audio recording and playback
- CloudKit for data storing (whistles audio + metadata) and for push notifications

Based on [this tutorial](https://www.hackingwithswift.com/read/33/overview) from Hacking with Swift, with some improvements after completion.