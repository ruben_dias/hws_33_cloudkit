//
//  Whistle.swift
//  Project33
//
//  Created by Ruben Dias on 04/02/2020.
//  Copyright © 2020 Ruben Dias. All rights reserved.
//

import UIKit
import CloudKit

class Whistle: NSObject {
    var recordID: CKRecord.ID!
    var genre: String!
    var comments: String!
    var audio: URL!
}
