//
//  MyGenresViewController.swift
//  Project33
//
//  Created by Ruben Dias on 04/02/2020.
//  Copyright © 2020 Ruben Dias. All rights reserved.
//

import UIKit
import CloudKit

class MyGenresViewController: UITableViewController {

    var myGenres: [String]!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        let defaults = UserDefaults.standard
        if let savedGenres = defaults.object(forKey: "myGenres") as? [String] {
            myGenres = savedGenres
        } else {
            myGenres = [String]()
        }

        title = "Notify me about…"
        navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Save", style: .plain, target: self, action: #selector(saveTapped))
        tableView.register(UITableViewCell.self, forCellReuseIdentifier: "Cell")
    }
    
    @objc func saveTapped() {
        let defaults = UserDefaults.standard
        defaults.set(myGenres, forKey: "myGenres")
        
        let database = CKContainer.default().publicCloudDatabase

        database.fetchAllSubscriptions { [unowned self] subscriptions, error in
            if error == nil {
                if let subscriptions = subscriptions {
                    for subscription in subscriptions {
                        database.delete(withSubscriptionID: subscription.subscriptionID) { str, error in
                            if error != nil {
                                let ac = UIAlertController(title: "Error", message: "There was a problem deleting subscription with id \(subscription.subscriptionID): \(error!.localizedDescription)", preferredStyle: .alert)
                                ac.addAction(UIAlertAction(title: "OK", style: .default))
                                self.present(ac, animated: true)
                            }
                        }
                    }
                }

                var completedWithoutErrors = true
                
                for genre in self.myGenres {
                    let predicate = NSPredicate(format:"genre = %@", genre)
                    let subscription = CKQuerySubscription(recordType: "Whistles", predicate: predicate, options: .firesOnRecordCreation)

                    let notification = CKSubscription.NotificationInfo()
                    notification.alertBody = "There's a new whistle in the \(genre) genre."
                    notification.soundName = "default"

                    subscription.notificationInfo = notification

                    database.save(subscription) { result, error in
                        if let error = error {
                            completedWithoutErrors = false

                            DispatchQueue.main.async {
                                let ac = UIAlertController(title: "Error", message: "There was a problem subscription subscribing for notifications for \(genre): \(error.localizedDescription)", preferredStyle: .alert)
                                ac.addAction(UIAlertAction(title: "OK", style: .default))
                                self.present(ac, animated: true)
                            }
                        }
                    }
                }

                if completedWithoutErrors {
                    DispatchQueue.main.async {
                        let ac = UIAlertController(title: "Success", message: "Your genre preferences were successfully saved", preferredStyle: .alert)
                        
                        let okAction = UIAlertAction(title: "OK", style: .default) { [unowned self] action in
                            self.navigationController?.popViewController(animated: true)
                        }
                        ac.addAction(okAction)
                        self.present(ac, animated: true)
                    }
                }
            } else {
                DispatchQueue.main.async {
                    let ac = UIAlertController(title: "Error", message: "There was a problem fetching notification subscriptions: \(error!.localizedDescription)", preferredStyle: .alert)
                    ac.addAction(UIAlertAction(title: "OK", style: .default))
                    self.present(ac, animated: true)
                }
            }
        }
    }
}

extension MyGenresViewController {
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return SelectGenreViewController.genres.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath)

        let genre = SelectGenreViewController.genres[indexPath.row]
        cell.textLabel?.text = genre

        if myGenres.contains(genre) {
            cell.accessoryType = .checkmark
        } else {
            cell.accessoryType = .none
        }

        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let cell = tableView.cellForRow(at: indexPath) {
            let selectedGenre = SelectGenreViewController.genres[indexPath.row]

            if cell.accessoryType == .none {
                cell.accessoryType = .checkmark
                myGenres.append(selectedGenre)
            } else {
                cell.accessoryType = .none

                if let index = myGenres.firstIndex(of: selectedGenre) {
                    myGenres.remove(at: index)
                }
            }
        }

        tableView.deselectRow(at: indexPath, animated: false)
    }
}
